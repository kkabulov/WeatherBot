# todo: logging, docstrings, poem

import random
import time
import urllib

from telepot import Bot
from telepot.loop import MessageLoop

from datetime import datetime
from requests import get

kozim_weather_bot_token = ''

class WeatherBot(Bot):
    google_images_key = ''
    google_search_engine = ''
    google_maps = ''
    open_weather_key = ''

    def __init__(self, token):
        super().__init__(token)
        self.time_name = None

    def parse_city(self, message):
        for w in message.split():
            if w[0] == '$':
                return w[1:].replace('_', ' ')
        raise ValueError(u"Какой город? Название города следует начинать с символа $")

    def handle(self, msg):
        chat_id = msg['chat']['id']
        message_text = msg['text']
        print(message_text)
        if message_text == '/start':
            self.sendMessage(chat_id, "Приветствую! Могу сообщить прогноз погоды")
            return
        elif message_text == '/help':
            self.sendMessage(chat_id,
                             u"Бот предоставляет информацию про погоду с картинками.\n"
                             u"Формат ввода\n"
                             u"1. [$Город время]\n"
                             u"2. [$Название_нижнее_подчеркивание_вместо_пробела время]\n"
                             u"Примеры запросов:\n"
                             u"'$Ульяновск завтра'\n"
                             u"'$New_York завтра'\n"
                             u"'$Караганда в среду'\n"
                             u"'$Бахчисарай погода в четверг'\n"
                             u"'погода $Анапа в пятницу\n")
            return
        else:
            try:
                city = self.parse_city(message_text)
                time = self.parse_time(message_text)
            except ValueError as value_error:
                self.sendMessage(chat_id, str(value_error))
                return

        weather = []
        try:
            weather = self.get_weather(city, time)
            self.sendMessage(chat_id, u"в городе {}{}{} {} градусов цельсия".format(
                city,
                self.get_time_name(time),
                weather['detail'][0],
                round(weather['temp'], 2),
            ))
        except BaseException as ex:
            self.sendMessage(chat_id,   u"Для города "
                                        u"{} прогноз сейчас недоступен, "
                                        u"что-то пошло не так "
                                        u"при выполнении запроса".format(city))
            print("get_weather exception: " + str(ex))

        try:
            photo = self.get_city_photo(city, weather)
            self.sendPhoto(chat_id, photo)
        except BaseException as ex:
            self.sendMessage(chat_id, u"Фотку не могу достать, "
                                      u"что-то пошло не так "
                                      u"при выполнении запроса")
            print("get_city_photo exception: " + str(ex))

    def parse_time(self, message_text):
        if u'послепослезавтра' in message_text:
            self.time_name = u' послепослезавтра '
            return 3
        if u'послезавтра' in message_text:
            self.time_name = u' послезавтра '
            return 2
        if u'завтра' in message_text:
            self.time_name = u' завтра '
            return 1
        weekdays = {
            u'понедельник': (0, u' в понедельник '),
            u'вторник': (1, u' во вторник '),
            u'сред': (2, u' в среду '),
            u'четверг': (3, u' в четверг '),
            u'пятниц': (4, u' в пятницу '),
            u'суббот': (5, u' в субботу '),
            u'воскрес': (6, u' в воскресенье ')
        }
        today = datetime.today().weekday()
        for day in weekdays:
            if day in message_text:
                self.time_name = weekdays[day][1]
                distance = weekdays[day][0] - today
                return distance if distance > 0 else distance + 7
        return None

    def _get_weather_no_time(self, city):
        weather_url = 'http://api.openweathermap.org/data/2.5/weather?{}'
        get_weather_url = weather_url.format(
            urllib.parse.urlencode(
                {
                    'q': city,
                    'appid': self.open_weather_key,
                    'lang': 'ru'
                }
            )
        )
        return get(get_weather_url).json()

    def _get_weather_with_time(self, city, time):
        forecast_url = 'http://api.openweathermap.org/data/2.5/forecast?{}'
        get_weather_forecast_url = forecast_url.format(
            urllib.parse.urlencode(
                {
                    'q': city,
                    'appid': self.open_weather_key,
                    'cnt': time + 1,
                    'lang': 'ru'
                }
            )
        )
        return get(get_weather_forecast_url).json()['list'][time]

    def get_weather(self, city, time):
        if time is None:
            weather = self._get_weather_no_time(city)
        else:
            weather = self._get_weather_with_time(city, time)

        return {
            'temp': weather['main']['temp'] - 273,
            'detail': [w['description'] for w in weather['weather']],
        }

    def get_city_photo(self, city, weather):
        query_text = city + " " + weather['detail'][0] + u' фото'
        get_photo_url = 'https://www.googleapis.com/customsearch/v1?{}'.format(
            urllib.parse.urlencode(
                {
                    'q': query_text,
                    'cx': self.google_images_key,
                    'searchType': 'image',
                    'key': self.google_search_engine
                }
            )
        )
        photos = get(get_photo_url).json()
        itempos = random.randint(0, len(photos['items']) - 1)
        return photos['items'][itempos]['link']

    def get_time_name(self, time):
        if time is None:
            return u' сейчас '
        if self.time_name is not None:
            return self.time_name
        return ''

cute_weather_bot = WeatherBot(kozim_weather_bot_token)

MessageLoop(cute_weather_bot, cute_weather_bot.handle).run_as_thread()

while True:
    time.sleep(15)
